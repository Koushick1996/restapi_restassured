package com.RestAssured_API_Utilites;

import org.apache.commons.lang3.RandomStringUtils;

import io.restassured.path.json.JsonPath;

public class Utilities 
{
	
	public static String Getname()
	{
		String Responsename = RandomStringUtils.randomAlphabetic(1);
		return("Koushick"+Responsename);
	}
	public static String Getjob()
	{
		String Responsename = RandomStringUtils.randomAlphabetic(2);
		return("TestEngineer"+Responsename);
	}
	public static String GetSalary()
	{
		String Responsesal = RandomStringUtils.randomNumeric(5);
		return Responsesal;
	}
	public static String Getage()
	{
		String Responseage = RandomStringUtils.randomNumeric(2);
		return Responseage;
	}

}
