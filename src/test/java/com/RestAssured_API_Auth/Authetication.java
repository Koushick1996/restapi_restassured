package com.RestAssured_API_Auth;

import io.restassured.RestAssured;
import io.restassured.http.Method;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.RestAssured_API_BaseTest.Base;

public class Authetication extends Base
{

	@Test
	void Authetication()
	{
		logger.info("************Checking Authetication*************");
		RestAssured.baseURI="http://restapi.demoqa.com";
		httpreq=RestAssured.given();
		httpreq.auth().preemptive().basic("ToolsQA", "TestPassword");
		response= httpreq.request(Method.GET,"/authentication/CheckForAuthentication");
		int statuscode = response.statusCode();
		logger.info(""+statuscode);
	}
	
	@Test(dependsOnMethods="Authetication")
	void Checkresponsebody()
	{
		String Responsebody = response.getBody().asString();
		logger.info("The Body is " + Responsebody);
		Assert.assertTrue(Responsebody!=null);
		
		logger.info("**********End of Testing*************");
	}
}
