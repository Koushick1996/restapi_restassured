package com.RestAssured_API_BaseTest;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class Base 
{
	public static RequestSpecification httpreq;
	public static Response response;
	
	public static Logger logger;
	
	@BeforeClass
	public void Setup()
	{
		logger = logger.getLogger("SetRestApi");
		PropertyConfigurator.configure("log4j.properties");
		logger.setLevel(Level.DEBUG);
	}

}
