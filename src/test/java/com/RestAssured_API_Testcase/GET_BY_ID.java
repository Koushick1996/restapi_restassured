package com.RestAssured_API_Testcase;

import io.restassured.RestAssured;
import io.restassured.http.Method;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import groovy.transform.BaseScript;

import com.RestAssured_API_BaseTest.Base;

public class GET_BY_ID extends Base
{
	POST obj = new POST();
	@BeforeClass
	void GetByEmployee()
	{
		logger.info("**********************GET By ID***************s");
		RestAssured.baseURI="http://dummy.restapiexample.com/api/v1";
		httpreq=RestAssured.given();
		String Empid = obj.Getid();
		response=httpreq.request(Method.GET,"/employee/"+Empid);
	}
	@Test
	void CheckResponseBody()
	{
		String Responnsebody = response.getBody().asString();
		logger.info("The Response body is "+Responnsebody);
		Assert.assertTrue(Responnsebody!=null);
	}
	

}
