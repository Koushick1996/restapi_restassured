package com.RestAssured_API_Testcase;

import io.restassured.RestAssured;
import io.restassured.http.Method;

import org.json.simple.JSONObject;
import org.testng.annotations.Test;

import com.RestAssured_API_BaseTest.Base;
import com.RestAssured_API_Utilites.Utilities;

public class PATCH extends Base
{
	
	String EmpAge = Utilities.Getage();
	
	POST obj = new POST();
	
	@Test
	void CreatenewEmployee() throws InterruptedException
	{
		logger.info("******************TC04 PATCH*****************");
		RestAssured.baseURI="http://dummy.restapiexample.com/api/v1";
		httpreq = RestAssured.given();
		
		JSONObject httpreqpara = new JSONObject();
		httpreqpara.put("age", EmpAge);

		httpreq.header("Content-Type","application/json");
		httpreq.body(httpreqpara.toJSONString());
		
	    String Empid = obj.Getid();
		
		response = httpreq.request(Method.PATCH,"/employee/"+Empid);
		
		Thread.sleep(5000);
	}
	
	@Test(dependsOnMethods="CreatenewEmployee")
	void Check_Response_Body()
	{
		String Responsebody = response.getBody().asString();
		logger.info("The Response body is " + Responsebody);
		
		org.testng.Assert.assertEquals(Responsebody.contains(EmpAge), true);

	}

}
