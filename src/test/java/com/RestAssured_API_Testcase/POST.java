package com.RestAssured_API_Testcase;


import junit.framework.Assert;
import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.path.json.JsonPath;

import org.json.simple.JSONObject;
import org.testng.annotations.Test;

import com.RestAssured_API_BaseTest.Base;
import com.RestAssured_API_Utilites.Utilities;

public class POST extends Base
{
	String Empname = Utilities.Getname();
	String EmpAge = Utilities.Getage();
	String Empsal = Utilities.GetSalary();
	
	@Test
	void CreatenewEmployee() throws InterruptedException
	{
		logger.info("******************TC02 POST******************");
		RestAssured.baseURI="http://dummy.restapiexample.com/api/v1";
		httpreq = RestAssured.given();
		JSONObject httpreqpara = new JSONObject();
		httpreqpara.put("name", Empname);
		httpreqpara.put("age", EmpAge);
		httpreqpara.put("salary", Empsal);
		
		httpreq.header("Content-Type","application/json");
		httpreq.body(httpreqpara.toJSONString());
		
		response = httpreq.request(Method.POST, "/create");
		
		Thread.sleep(5000);
	}
	
	@Test(dependsOnMethods="CreatenewEmployee")
	void Check_Response_Body()
	{
		String Responsebody = response.getBody().asString();
		logger.info("The Response body is " + Responsebody);
		
		org.testng.Assert.assertEquals(Responsebody.contains(Empname), true);
		org.testng.Assert.assertEquals(Responsebody.contains(EmpAge), true);
		org.testng.Assert.assertEquals(Responsebody.contains(Empsal), true);
	}
	
	@Test(dependsOnMethods="Check_Response_Body")
	String Getid()
	{
		JsonPath randomnumber = response.jsonPath();
		String Employeeid = randomnumber.getString("id");
		logger.info("The Employee id is : " + Employeeid);
		return Employeeid;
	}
	@Test(dependsOnMethods="Check_Response_Body")
	void Check_Status_Code()
	{
		int Responsestatus = response.getStatusCode();
		logger.info("The Response status " + Responsestatus);
		org.testng.Assert.assertEquals(Responsestatus, 200);
	}
    @Test(dependsOnMethods="Check_Status_Code")
    void Check_Content_type()
    {
    	String Responsecontent = response.header("Content-Type");
    	logger.info("The Response Content " + Responsecontent);
    	org.testng.Assert.assertEquals(Responsecontent, "text/html; charset=UTF-8");
    }
    @Test(dependsOnMethods="Check_Content_type")
    void Check_Server()
    {
    	String responseserver = response.header("Server");
    	logger.info("The Response Server " + responseserver);
    	org.testng.Assert.assertEquals(responseserver, "nginx/1.14.1");
    }
}
