package com.RestAssured_API_Testcase;

import io.restassured.RestAssured;
import io.restassured.http.Method;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.RestAssured_API_BaseTest.Base;

public class DELETE extends Base
{
	POST obj = new POST();

	@BeforeClass
	void GetByEmployee()
	{
		logger.info("**********************DELETE EMPLOYEE***************s");
		RestAssured.baseURI="http://dummy.restapiexample.com/api/v1";
		httpreq=RestAssured.given();
		String Empid = obj.Getid();
		response=httpreq.request(Method.DELETE,"/delete/"+Empid);
	}
	@Test
	void CheckResponseBody()
	{
		String Responnsebody = response.getBody().asString();
		logger.info("The Response body is "+Responnsebody);
		Assert.assertTrue(Responnsebody!=null);
	}
	@Test(dependsOnMethods="CheckResponseBody")
	void Checkstatuscode()
	{
		int statuscode = response.getStatusCode();
		logger.info("The status code is " + statuscode);
		org.testng.Assert.assertEquals(statuscode, 200);
		
	}

}
