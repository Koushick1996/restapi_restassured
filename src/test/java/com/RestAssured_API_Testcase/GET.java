package com.RestAssured_API_Testcase;

import io.restassured.RestAssured;
import io.restassured.http.Method;

import org.junit.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.RestAssured_API_BaseTest.Base;

public class GET extends Base {
	
	@BeforeClass
	void GetAllEmployees()
	{
		logger.info("**************Start of Testing**********");
		logger.info("******************TC01 Get**************");
		RestAssured.baseURI="http://dummy.restapiexample.com/api/v1";
		httpreq = RestAssured.given();
		response=httpreq.request(Method.GET, "/employees");
	}
	@Test
	void CheckResponseBody()
	{
		String responsebody = response.getBody().asString();
		//String Responseheaders = response.getHeaders().toString();
		logger.info("The Body is " + responsebody);
		//logger.info("" + Responseheaders);
		org.testng.Assert.assertTrue(responsebody!=null);
	}
	@Test(dependsOnMethods="CheckResponseBody")
	void Checkstatuscode()
	{
		int statuscode = response.getStatusCode();
		logger.info("The status code is " + statuscode);
		org.testng.Assert.assertEquals(statuscode, 200);
		
	}
	@Test
	void CheckresponseTime()
	{
		logger.info("************Checking response Time**********");
		long responsetime = response.getTime();
		logger.info("The Response Time is " + responsetime);
		if(responsetime>2000)
		{
			logger.warn("The Response time is Greater than 2000");
		}
		Assert.assertTrue(responsetime<10000);
	}
	@Test
	void CheckstatusLine()
	{
		logger.info("************Checking status Line**********");
		
		String Statusline = response.getStatusLine();
		logger.info("The Status Line is " + Statusline);
		Assert.assertEquals(Statusline, "HTTP/1.1 200 OK");
	}
	@Test
	void CheckContenttype()
	{
		logger.info("************Checking Content Type**********");
		
		String ContentType = response.header("Content-Type");
		logger.info("The Content Type is " + ContentType);
		Assert.assertEquals(ContentType, "text/html; charset=UTF-8");
		
	}
	@Test
	void Checkresponseheader()
	{
		logger.info("************Checking Response header**********");
		
		String Responseheader = response.header("Server");
		logger.info("The Response header is " + Responseheader);
		Assert.assertEquals(Responseheader, "nginx/1.14.1");
	}
	
	@Test
	void CheckResponseheader_Content()
	{
		logger.info("************Checking Response header content**********");
		
		String Responseheader_content = response.header("Content-Encoding");
		logger.info("The Response header is " + Responseheader_content);
		Assert.assertEquals(Responseheader_content, "gzip");
	}
	@Test
	void CheckResponseheader_Content_Length()
	{
		logger.info("************Checking Response header Content Length**********");
		String Responseheader_content_Length = response.header("Content-Length");
		logger.info("The Response header is " + Responseheader_content_Length);
		Assert.assertEquals(Responseheader_content_Length, "762");
	}

}
