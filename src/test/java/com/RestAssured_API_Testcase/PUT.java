package com.RestAssured_API_Testcase;

import io.restassured.RestAssured;
import io.restassured.http.Method;

import org.json.simple.JSONObject;
import org.testng.annotations.Test;

import com.RestAssured_API_BaseTest.Base;
import com.RestAssured_API_Utilites.Utilities;

public class PUT extends Base
{
	String Empname = Utilities.Getname();
	String EmpAge = Utilities.Getage();
	String Empsal = Utilities.GetSalary();
	
	POST obj = new POST();
	
	@Test
	void CreatenewEmployee() throws InterruptedException
	{
		logger.info("******************TC04 PUT*****************");
		RestAssured.baseURI="http://dummy.restapiexample.com/api/v1";
		httpreq = RestAssured.given();
		JSONObject httpreqpara = new JSONObject();
		httpreqpara.put("name", Empname);
		httpreqpara.put("age", EmpAge);
		httpreqpara.put("salary", Empsal);
		
		httpreq.header("Content-Type","application/json");
		httpreq.body(httpreqpara.toJSONString());
		
	    String Empid = obj.Getid();
		
		response = httpreq.request(Method.PUT,"/update/"+Empid);
		
		Thread.sleep(5000);
	}
	
	@Test(dependsOnMethods="CreatenewEmployee")
	void Check_Response_Body()
	{
		String Responsebody = response.getBody().asString();
		logger.info("The Response body is " + Responsebody);
		
		org.testng.Assert.assertEquals(Responsebody.contains(Empname), true);
		org.testng.Assert.assertEquals(Responsebody.contains(EmpAge), true);
		org.testng.Assert.assertEquals(Responsebody.contains(Empsal), true);
	}
}
